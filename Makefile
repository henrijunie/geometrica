all:	geometrica.o retangulo.o triangulo.o main.o
		g++ -o main geometrica.o triangulo.o retangulo.o main.o 

geometrica.o:	geometrica.cpp geometrica.hpp
				g++ -c geometrica.cpp

triangulo.o:	triangulo.cpp triangulo.hpp geometrica.hpp
				g++ -c triangulo.cpp

retangulo.o:	retangulo.cpp retangulo.hpp geometrica.hpp
				g++ -c retangulo.cpp

main.o:			main.cpp retangulo.hpp triangulo.hpp
				g++ -c main.cpp

clean:
	rm -rf *.o

run:
	./main
