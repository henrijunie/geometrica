#include "retangulo.hpp"
#include "triangulo.hpp"
#include "quadrado.hpp"
#include <iostream>

using namespace std;

int main(){

	geometrica * formaGeometrica = new retangulo(20,20);
	cout << "Calculo da Area do retangulo: " << formaGeometrica->getBase() << "x" << formaGeometrica->getAltura() << "=" << formaGeometrica->area() << endl;

	geometrica * formaGeometricaT = new triangulo(20,20);
	cout << "Calculo da Area do triangulo: " << formaGeometricaT->getBase() << "x" << formaGeometricaT->getAltura() << "=" << formaGeometricaT->area() << endl;

	geometrica * formaGeometricaQ = new quadrado(20);
	cout << "Calculo da Area do quadrado: " << formaGeometricaQ->getBase() << "x" << formaGeometricaQ->getAltura() << "=" << formaGeometricaQ->area() << endl;

	return 0;
}
